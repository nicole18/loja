<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        $this->form_validation->set_rules('cep', 'CEP destino', 'trim|required|exact_length[9]');
        $this->form_validation->set_rules('produto_id', 'Produto ID', 'trim|required');

        $retorno = array();

        if($this->form_validation->run()){

            $produto_id = (int) $this->input->post('produto_id');

            if(!$produto = $this->core_model->get_by_id('produtos', array('produto_id' => $produto_id))){

                $retorno['erro'] = 3;
                $retorno['retorno_endereco'] = 'Não encontramos o produto em nossa base de dados';
                echo json_encode($retorno);
                exit();
            
            }else{

                //inicio da consulta ao web service via CEP


                //sucesso... Continua o processamento

                $cep_destino = str_replace('-', '', $this->input->post('cep'));

                // -> https://viacep.com.br/ws/07145480/json/
    
                //montando a URL para consultar o endereço
                $url_endereco = 'https://viacep.com.br/ws/';
                $url_endereco .= $cep_destino;
                $url_endereco .= '/json/';
    
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_URL, $url_endereco);
    
                $resultado = curl_exec($curl);
        
                $resultado = json_decode($resultado);
    
                if(isset($resultado->erro)){
                    $retorno['erro'] = 3;
                    $retorno['mensagem'] = 'Não encontramos o CEP em nossa base de dados';
                    $retorno['retorno_endereco'] = 'Não encontramos o CEP em nossa base de dados';

                    echo json_encode($retorno);
                    exit;

                }else{
    
                    $retorno['erro'] = 0;
                    $retorno['mensagem'] = 'Sucesso';
                    $retorno_endereco = $retorno['retorno_endereco'] = $resultado->logradouro . ', '.$resultado->bairro. ', '.$resultado->localidade. ' - '. $resultado->uf . ', '. $resultado->cep;
                }

                //-------------Final da consulta ao web service via CEP

                //Inicio da consulta ao web service dos correios

                // --> Montando a URL para os correios exibir o valor do frete
            
                //Informações dos correios no banco de dados
                $config_correios = $this->core_model->get_by_id('config_correios', array('config_id' => 1));

                //para onde será enviado o produto
                $cep_destino = $this->input->post('cep');
                
                $url_correios = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
                $url_correios .= 'nCdEmpresa=08082650';
                $url_correios .= '&sDsSenha=564321';
                $url_correios .= '&sCepOrigem=' . str_replace('-', '', $config_correios->config_cep_origem);
                $url_correios .= '&sCepDestino=' . str_replace('-', '', $cep_destino);
                $url_correios .= '&nVlPeso=' . $produto->produto_peso;
                $url_correios .= '&nCdFormato=1';
                $url_correios .= '&nVlComprimento='. $produto->produto_comprimento;
                $url_correios .= '&nVlAltura=' . $produto->produto_altura;
                $url_correios .= '&nVlLargura=' . $produto->produto_largura;
                $url_correios .= '&sCdMaoPropria=n';
                $url_correios .= '&nVlValorDeclarado=' . $config_correios->config_valor_declarado;
                $url_correios .= '&sCdAvisoRecebimento=n';
                $url_correios .= '&nCdServico='.$config_correios->config_codigo_pac;
                $url_correios .= '&nCdServico='.$config_correios->config_codigo_sedex;
                $url_correios .= '&nVlDiametro=0';
                $url_correios .= '&StrRetorno=xml';
                $url_correios .= '&nIndicaCalculo=3';

                //echo json_encode($url_correios);
                //exit();

                //Transformando o XML em um objeto
                $xml = simplexml_load_file($url_correios);
                $xml = json_encode($xml);

                $consulta = json_decode($xml);

                //garantindo que houve sucesso na consulta ao web service dos correios
                if($consulta->cServico[0]->Valor == '0,00'){

                    $retorno['erro'] = 3;
                    $retorno['retorno_endereco'] = 'Não foi possível calcular o frete. Por favor entre em contato com nosso suporte';
                    exit();

                }else{

                    $frete_calculado = "";

                    foreach($consulta->cServico as $dados){

                        $valor_formatado = str_replace(',', '.', $dados->Valor);
                        
                        number_format($valor_calculado = ($valor_formatado + $config_correios->config_somar_frete), 2, '.', '');
                        
                        $frete_calculado .= '<p>'.($dados->Codigo == '04510' ? 'PAC' : 'Sedex' ).' &nbsp;R$&nbsp;'.$valor_calculado.', <span class="badge badge-primary">'.($dados->PrazoEntrega == '1' ? $dados->PrazoEntrega.'&nbsp;</span> dia útil' : $dados->PrazoEntrega.'&nbsp;</span> dias úteis' ).'</p>';

                    }

                }

                $retorno['erro'] = 0;
                $retorno['retorno_endereco'] = $retorno_endereco .'<br/><br/>'. $frete_calculado;

            }

        }else{

            //erro de validação
            $retorno['erro'] = 5;
            $retorno['retorno_endereco'] = validation_errors();

            echo json_encode($retorno);
            exit;

        }

        echo json_encode($retorno);
    }

}